const express = require("express");
var bodyParser = require('body-parser')
const cors = require("cors");
const Conexion  = require("./conexion_mysql");

const app = express();

app.use(cors());

app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded({extended:true})); // to support URL-encoded bodies


let respuesta = {
    success: true,
    code: 200,
    message: ''
   };



app.get('/', function (req,res){
    res.send('Welcome')
})


/**END POINT PARA OBTENER LA ESPECIALIDADES */
app.get('/especialidades',async function(req,res){
    
    var especialidades = await Conexion.Consulta('Select * from especialidades'); 

    respuesta = {
        success: true,
        code: 200,
        message: 'especialidades',
        data: especialidades
    };

    res.json(respuesta);

});


/**ENDPOINT PARA REGISTRAT UN USUARIO NUEVO */
app.post('/signup', function(req,res){
    console.log(req);

    respuesta = {
        success : false,
        code : 200,
        message : 'signup',
        data : []
    }
    res.json(respuesta)
});

app.listen(3000, () => {
    console.log("El servidor esta iniciado en el puerto 3000")
});