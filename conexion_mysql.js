var mysql = require('mysql');

var conexion = mysql.createConnection({
    host : '127.0.0.1',
    database : 'jam_tech',
    user : 'root',
    password : ''
});

function AbrirConexion() {
    conexion.connect(function(err){
        if(err)
        {
            console.error('Error de conexion: ' + err.stack);
            return;
        }
    
        console.log('Conectado con el identificador ' + conexion.threadId);
    });
}

function CerrarConexion() {
    conexion.end();
}


const doQuery = (consulta) => {
    return new Promise((resolve,reject) => {
        conexion.query(consulta, function(error,results,fileds){
            if(error)
                throw error;
            else
            {
                return resolve(results);
            }
        });
    })
}


/**
 * funcion para realizar consultas a la base de datos
 * @returns {result}
 */
exports.Consulta = async (consulta) => {
    const result = await doQuery(consulta);
    return result
}

//export { Consulta };