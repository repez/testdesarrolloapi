/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50724
Source Host           : 127.0.0.1:3306
Source Database       : jam_tech

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-08-31 12:07:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for especialidades
-- ----------------------------
DROP TABLE IF EXISTS `especialidades`;
CREATE TABLE `especialidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `estatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of especialidades
-- ----------------------------
INSERT INTO `especialidades` VALUES ('1', 'Especialidad 1', 'Especialidad 1', '1');
INSERT INTO `especialidades` VALUES ('2', 'Especialidad 2', 'Especialidad 2', '1');
INSERT INTO `especialidades` VALUES ('3', 'Especialidad 3', 'Especialidad 3', '1');
INSERT INTO `especialidades` VALUES ('4', 'Especialidad 4', 'Especialidad 4', '1');
INSERT INTO `especialidades` VALUES ('5', 'Especialidad 5', 'Especialidad 5', '1');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `delivery` tinyint(1) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `estatus` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of usuarios
-- ----------------------------

-- ----------------------------
-- Table structure for usuario_especialidad
-- ----------------------------
DROP TABLE IF EXISTS `usuario_especialidad`;
CREATE TABLE `usuario_especialidad` (
  `idUsuario` int(11) DEFAULT NULL,
  `idEspecialidad` int(11) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of usuario_especialidad
-- ----------------------------

-- ----------------------------
-- Table structure for usuario_lugar
-- ----------------------------
DROP TABLE IF EXISTS `usuario_lugar`;
CREATE TABLE `usuario_lugar` (
  `idUsuario` int(11) DEFAULT NULL,
  `lugar` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of usuario_lugar
-- ----------------------------
